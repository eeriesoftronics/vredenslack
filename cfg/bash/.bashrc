# ~/.bashrc: executed by bash(1) for non-login shells.

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# don't put duplicate lines in the history. See bash(1) for more options
# don't overwrite GNU Midnight Commander's setting of `ignorespace'.
HISTCONTROL=$HISTCONTROL${HISTCONTROL+:}ignoredups
# ... or force ignoredups and ignorespace
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
	test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
	alias ls='ls --color=auto'
	#alias dir='dir --color=auto'
	#alias vdir='vdir --color=auto'

	#alias grep='grep --color=auto'
	#alias fgrep='fgrep --color=auto'
	alias egrep='egrep --color=auto'
fi

# make less more friendly for non-text input files, see lesspipe(1)
#[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

__git_fetch_branch_commit_or_tag() {
 a=$(git branch 2>/dev/null | grep '' | sed 's/ //')
 if [ ! -z "$a" ]; then
       if [ "$a" = '(no branch)' ]; then
               b=$(git log -n 1 | grep 'commit' | sed 's/commit //')
               c=$(git tag --contains $b | head -n 1)
               xtra=''
               xtra="($b"
               if [ ! -z "$c" ]; then
                 xtra=$xtra"|tag:$c"
               fi
               echo $xtra")"
       else
               echo "($a)"
       fi
 fi
}

PS1="\n\[\e[32;1m\](\[\e[37;1m\]\u@\h\[\e[32;1m\])-(\[\e[37;1m\]jobs:\j\[\e[32;1m\])-(\[\e[37;1m\]\w\[\e[32;1m\]) \[\e[33;1m\]$(__git_fetch_branch_commit_or_tag)\[\e[32;1m\]\n(\[\[\e[37;1m\]\t\[\e[32;1m\])-> \[\e[0m\]"
export PS1

# aliases
[ -f ~/.bash_aliases ] && . ~/.bash_aliases

# more bash initializations (to avoid replicating things in .bash_profile and .bashrc)
[ -f ~/.bash_ext ] && . ~/.bash_ext
