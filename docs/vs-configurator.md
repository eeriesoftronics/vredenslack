# NAME

vs-configurator - Process templates of configuration files and produce final versions

# DESCRIPTION

This script will read translations in the form of key-values and attempt to replace placeholders containing references to said keys with the respective values. This is useful for creating template configuration files with sensitive information such as passwords or other values which depend on the environment the program is suppose to run.

A **translation** is our chosen term for indicating a key-value tupple. A **placeholder** is a special sequence of characters which can reference one or more keys (as well as an optional default value if no translation for a key exists), the first key for which there is a translation will replace the whole placeholder with the key's value. If no translation is found then an error will be issued and the output file will not be written/modified.

## Features

- Support for multiple configuration keys on a single placeholder in order to provide fallbacks
- Support for default values
- Support for environment variables

# SYNOPSIS

Check if there are translations for all placeholders of a template

    vs-configurator [OPTIONS] check <input file>

Process a template file and write the output to a file

    vs-configurator [OPTIONS] translate <input file> <output file>

List all translations

    vs-configurator [OPTIONS] list

Test the configurator against a sample text. Can also be useful to print information from the translation files.

    vs-configurator [OPTIONS] test '<sample text>'

# OPTIONS

## --ignore-failed

Passing this flag will ignore any missed translation when parsing a template file. This means that the program will terminate with success (exitcode 0) even if some translations were not found. Otherwise, any missing translation will result in the program terminating in error (exitcode 1).

Note that this simply controls the exitcode for when there is a missing translation, the output file will alwas be written. If you wish to run a check before re-creating the output file then run the **check** action first. The output file will keep any placeholder for which there was no translation found. This means there is no translation into an empty string. Bare this in mind since this can lead to potential configuration failures. This flag is mostly aimed at meta-templating.

## -l, --load &lt;file>

Load a translations file. See section TRANSLATION FILES for more info.

## --no-env

Do not load environment variables as translations.

## --quiet

Hides most STDOUT output. Note that actions which imply some for of output will restrict it to the bare minimum. For example, the _test_ action will only output the translated string, which lets you use it on a script to fetch a translation value.

## -t, --translation &lt;key>=&lt;value>

This allows you set a configuration key and its value. For example, `--translation my.server.hostname=localhost` would allow replacing all placeholders containing the reference `config.my.server.hostname` with the value `localhost`. Note that when specifying the translation you **must not** include the `config.` prefix.

# TEMPLATING

The templating system is the most basic placeholder templating system possible. In fact, the goal is that a single regular expression is capable of extracting the placeholder and once a suitable translation is found replace the placeholder with the final value.

Placeholder format: `{%config.key1:config.key2:env.ENV_VAR:-default value%}`

Configuration key references inside a placeholder must be preceeded by `config.`, e.g.: `{%config.my-app.server.hostname%}`

Configuration keys must match the following regular expression: `/\w+([-\.]\w+)*/`

References to environment variables must be preceeded by `env.`, e.g.: `{%env.HOME%}`.

Default values can contain any contain any printable character sequence with the exception of newline and the placeholder terminator character sequence `%}`.

You can escape the opening of a placeholder by using `\{%`.

## Example

Lets take the input file `input.conf.dist` with the following contents.

        hostname={%config.host1:config.host2:-localhost%}
        user_folder={%config.folder:env.HOME%}

If you run

    vs-configurator -t host2=127.0.0.1 input.conf.dist input.conf

The output file `input.conf` will contain

        hostname={%config.host1:config.host2:-localhost%}
        user_folder={%config.folder:env.HOME%}

Note that when you specify a translation you **must not** include the `config.` prefix. This is automatic for user specified translations. This is so that you don't accidentally override an environmental variable translations, which are automatically loaded. Also, future automatic translations might require other namespaces.

# TRANSLATION FILES

Instead of providing the configuration key-values as command line arguments you can add them to one or more files and feed these configuration files to the configurator program. For example, create the file `configurator.cfg` (the file extension must be .cfg) and add the following to its contents:

        ; this is a comment
        # this is also a comment
        # note that there is no space between the '=' and the value,
        # if you add a space the value will contain that space
        my-app.name=Some application
        my-app.sql.username=myapp
        my-app.sql.password=myapppwd
        ; note that we are now using ':' instead of '=' to separate the key from the value
        ; both symbols are supported
        sql.hostname:localhost
        sql.username:dbadmin
        sql.password:dbadminpwd

You can now load this configuration file into the configurator and test a placeholder string

    vs-configurator --load configurator.cfg test '{%config.my-app.sql.hostname:config.sql.hostname%}'

or you can list all the available configuration keys and values

    vs-configurator --load configurator.cfg list

# AUTHOR

JB Ribeiro (Vredens) - &lt;vredens@gmail.com>

# COPYRIGHT AND LICENSE

Copyright 2016 JB Ribeiro.

This program is free software; you can redistribute it and/or modify it under the same terms as Perl itself.

The full text of the license can be found in [http://dev.perl.org/licenses/](http://dev.perl.org/licenses/).
