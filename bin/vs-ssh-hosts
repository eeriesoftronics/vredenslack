#!/usr/bin/env perl

use 5.016;
use strict;
use warnings;

use File::Copy;
use Getopt::Long;
use Pod::Usage;

my $opts = {
	'dry-run' => 0,
};
GetOptions($opts,
	'dry-run!',
	'filter=s@',
);

my $a = shift;
my $actions = {
	'remove' => \&_remove,
	'rem'    => \&_remove,
	'delete' => \&_remove,
	'del'    => \&_remove,
	'list'   => \&_list,
};

pod2usage() unless defined $a and defined $actions->{$a};

&{$actions->{$a}}(@ARGV);

sub _remove {
	my @hosts = @_;

	my @oldhosts = load();
	my @newhosts;

	for my $h (@oldhosts) {
		if (grep { $h =~ /^$_/ } @hosts) {
			print 'removing: ', $h, "\n";
		} else {
			push @newhosts, $h;
		}
	}

	unless ($opts->{'dry-run'}) {
		store(@newhosts);
	}
}

sub _list {
	my @filters;

	@filters = @{$opts->{filter}} if defined $opts->{filter};

	say join(', ', @filters);

	my @hosts = load();

	foreach (@hosts) {
		my @params = split /\s+/;
		my @key = split //, $params[2];
		if (@filters) {
			...
		} else {
			printf "%30s ...%s\n", $params[0], join('', @key[-10..-1]);
		}
	}
}

sub store {
	my @hosts = @_;

	copy("$ENV{HOME}/.ssh/known_hosts", "$ENV{HOME}/.ssh/known_hosts.bkp") or die 'Failed to create backup file';

	open(my $fh, '>', "$ENV{HOME}/.ssh/known_hosts") or die 'failed to write to the known hosts file';
	foreach (@hosts) {
		print $fh $_, "\n";
	}
	close($fh);
}

sub load {
	my @hosts;

	open(my $fh, '<', "$ENV{HOME}/.ssh/known_hosts") or die 'failed to open';
	while (<$fh>) {
		chomp;
		push @hosts, $_;
	}
	close($fh);

	return @hosts;
}

__END__

=head1 NAME

vs-ssh-hosts - Manage SSH known hosts for your user

=head1 SYNOPSIS

This script allows you to quickly remove known hosts from your known_hosts file.

=head2 Remove a host from the list

  vs-ssh-hosts del <host name>

=head2 Remove a set of hosts using a regex

  vs-ssh-hosts del 192.168.*

=head2 List hosts

  vs-ssh-hosts list

=head2 List hosts which match one or more of the given filters (WIP)

  vs-ssh-hosts list --filter '192\.168\..*' --filter '.*\.local'

=head1 OPTIONS

=head2 --no-update

This will disable fetching updated from the central repository.

=head2 --no-interactive

This will disable interactive mode which is the same as answering 'Yes' to all
prompts.

=head2 --debug

This will enable debug mode which will make the installation script skip any
actual action.

=head2 --man

Show the complete manual for this script.

=head2 --help

A quick help.

=head1 AUTHOR

JB Ribeiro (Vredens) - E<lt>vredens@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2016 JB Ribeiro.

This program is free software; you can redistribute it and/or modify it under the same terms as Perl itself.

The full text of the license can be found in L<http://dev.perl.org/licenses/>.

=cut
