#!/usr/bin/env perl

use strict;
use warnings;

use File::Spec;
use Getopt::Long;
use Pod::Usage;
use Term::ANSIColor ':constants';

my $opts = {
	'compression' => 'bzip', # TODO: use different compressions
};
GetOptions($opts,
	'folder|f=s',
	'dry-run|d',
	'help'
);

pod2usage(-exitval => 0, -verbose => 1) if $opts->{help};

(my $folder = shift) =~ s!/+$!!;
die RED, 'Invalid argument, must be a folder or file. Use --help for more info.', RESET unless defined $folder and (-d $folder or -f $folder);

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime;
my $timestamp = sprintf('%d%02d%02d%02d%02d%02d', 1900 + $year, $mon + 1, $mday, $hour, $min, $sec);

my $filename = $folder . '-' . $timestamp . '.tar.bz2';
if ($opts->{folder}) {
	die 'The folder you passed with the --folder option does not exist or is not a directory' unless -d $opts->{folder};
	$filename = File::Spec->catfile($opts->{folder}, $filename);
}

run ('tar', '-cjf', $filename, $folder);

################################################################################
sub run {
################################################################################
	print BLUE, 'Running: [', CYAN, join(' ', @_), BLUE, ']', RESET, "\n";
	print '-' x 80, "\n";
	my $ec = 0;
	$ec = system (@_) unless $opts->{'dry-run'};
	print '-' x 80, "\n";
	$ec == 0;
}

__END__


=head1 NAME

vs-backup - Backup a file/folder

=head1 SYNOPSIS

This tool aims at making a simple backup as easy as possible. It will compress
into a timestamped file and, optionally, send the resulting file to a folder of
your choosing.

  vs-backup <file or folder>
  vs-backup <file or folder> --folder <backups folder>

=head1 OPTIONS

=head2 -f, --folder <folder>

This lets you specify a backup folder where to store your backup file.

=head2 -h, --help

This help

=head1 AUTHOR

JB Ribeiro (Vredens) - E<lt>vredens@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2016 JB Ribeiro.

This program is free software; you can redistribute it and/or modify it under the same terms as Perl itself.

The full text of the license can be found in L<http://dev.perl.org/licenses/>.

=cut
