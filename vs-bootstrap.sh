#!/bin/bash

set -e

root_folder=${HOME}/.vredenslack

if [ "$(id -u)" != "0" ]; then
	root_folder=/usr/local/libexec/vredenslack
fi

if [ ! -d ${root_folder} ]; then
	echo "Creating folder: ${root_folder}"
	mkdir ${root_folder}
fi

pushd ${root_folder}

if [ ! -d vscripts ]; then
	echo "Cloning vredenslack repository to ${HOME}/.vredenslack/vslack"
	git clone https://bitbucket.org/eeriesoftronics/vredenslack.git vscripts
	pushd vscripts
else
	pushd vscripts
	git pull
fi

echo "Running [${root_folder}/vscripts/bin/vs-self install vscripts] which will install the tool scripts"
./bin/vs-self install vscripts $*
